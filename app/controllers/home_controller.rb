class HomeController < ApplicationController
  require 'dijkstra'
  require 'drivers'
  include Dijkstra
  include Driver

  def index

  end

  def get_path(parents,dst,results)
    results.push(dst)
    if parents[dst] == -1
      return results
    else
      get_path(parents,parents[dst],results)
    end
  end
  # Euclidian Distance to find closest driver
  def find_distance(x1,x2,y1,y2)
    sum_of_squares = ((x1.to_f-x2.to_f)**2)+((y1.to_f-y2.to_f)**2)
    Math.sqrt( sum_of_squares )
  end
  def find_ride
    drivers = []
    driver_1 = Driver.new('Han Solo','015895656',1,0)
    driver_2 = Driver.new('Luke Skywalker','015895656',3,0)
    driver_3 = Driver.new('Darth Vader','015895656',0,4)
    drivers.push driver_1
    drivers.push driver_2
    drivers.push driver_3

    x_cod_user = params[:x_cod]
    y_cod_user = params[:y_cod]
    user_from = params[:from_location].to_i
    user_to = params[:to_location].to_i
    @closest_driver = nil
    @closest_distance = 999999999 #Infinite
    drivers.each do |driver|
      if @closest_driver.nil?
        @closest_driver = driver
        @closest_distance = find_distance(x_cod_user,driver.x,y_cod_user,driver.y)
      elsif find_distance(x_cod_user,driver.x,y_cod_user,driver.y)< @closest_distance
        @closest_distance = find_distance(x_cod_user,driver.x,y_cod_user,driver.y)
        @closest_driver = driver
      end
    end

    graph = Graph.new
    (1..6).each {|node| graph.push node }
    graph.connect_mutually 1, 2, 7
    graph.connect_mutually 1, 3, 9
    graph.connect_mutually 1, 6, 14
    graph.connect_mutually 2, 3, 10
    graph.connect_mutually 2, 4, 15
    graph.connect_mutually 3, 4, 11
    graph.connect_mutually 3, 6, 2
    graph.connect_mutually 4, 5, 6
    graph.connect_mutually 5, 6, 9

    # p graph
    # p graph.length_between(2, 1)
    # p graph.neighbors(1)
    paths =  graph.dijkstra(user_from, user_to)
    @result = get_path(paths,user_to,[])
    @result.reverse!
  end

end
