module Driver
  class Driver
    def initialize(name,phone,x,y)
      @name = name
      @phone = phone
      @x = x
      @y = y
    end
    def name
      @name
    end
    def x
      @x
    end
    def y
      @y
    end
    def phone
      @phone
    end
  end
end